#!/bin/sh

# E' necessario abilitare pacman ad essere eseguito senza password

icon="/usr/share/icons/Tela/scalable/apps/system-restart.svg"
repo=$(pacman -Qu | wc -l)
aur=$(paru -Qua | wc -l)

if [[ $repo -eq 0 ]] && [[ $aur -eq 0 ]]
then
    notify-send -i $icon 'RILASSATI' 'Non ci sono aggiornamenti'
fi
if [[ $repo -ne 0 ]] && [[ $aur -eq 0 ]]
then
    sudo pacman -Syu
    notify-send -i $icon 'REPO' 'Aggiornamenti effettuati'
fi
if [[ $repo -eq 0 ]] && [[ $aur -ne 0 ]]
then
    paru -Sua --skipreview
    notify-send -i $icon 'AUR' 'Aggiornamenti effettuati'
fi 
if [[ $repo -ne 0 ]] && [[ $aur -ne 0 ]]
then
    sudo pacman -Syu
    paru -Sua --skipreview
    notify-send -i $icon 'REPO+AUR' 'Aggiornamenti effettuati'
fi
polybar-msg hook updates 1
