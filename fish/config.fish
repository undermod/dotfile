if status is-interactive
    # Commands to run in interactive sessions can go here
end

set PATH /usr/sbin $PATH
starship init fish | source

