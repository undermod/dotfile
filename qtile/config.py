# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

########################################################################
###   LIBRARY IMPORT
########################################################################

import os
import re
import socket
import subprocess

from libqtile.config import KeyChord, Key, Screen, Group, Drag, Click, ScratchPad, DropDown, Match
from libqtile.command import lazy
from libqtile import layout, bar, widget, hook
from libqtile.lazy import lazy
from libqtile import qtile
from typing import List  # noqa: F401

########################################################################
###   VARIABLE
########################################################################

mod = "mod4"
alt = "mod1"
terminal = "alacritty"
browser = "firefox"
filemanager = "thunar"
dmenu = "dmenu_run -p 'Cerca : ' -fn 'Roboto-10' -nb '#313135' -nf '#5AF78E' -sb '#313135' -sf '#FF79C6'"
menu = "rofi -show drun --show-icon"
lock = "betterlockscreen -l dim"

########################################################################
###   KEYBINDING
########################################################################

keys = [

    # Switch between groups
    Key([alt], "Right", lazy.screen.next_group(), desc="Move to next group"),
    Key([alt], "Left", lazy.screen.prev_group(), desc="Move to previous group"),

    # Switch between windows
    Key([mod], "Left", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "Right", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "Down", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "Up", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "KP_Add", lazy.layout.next(), desc="Move window focus to other window"),

    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "Left", lazy.layout.shuffle_left(), desc="Move window to the left"),
    Key([mod, "shift"], "Right", lazy.layout.shuffle_right(), desc="Move window to the right"),
    Key([mod, "shift"], "Down", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "Up", lazy.layout.shuffle_up(), desc="Move window up"),

    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"], "Left", lazy.layout.grow_left(), desc="Grow window to the left"),
    Key([mod, "control"], "Right", lazy.layout.grow_right(), desc="Grow window to the right"),
    Key([mod, "control"], "Down", lazy.layout.grow_down(), desc="Grow window down"),
    Key([mod, "control"], "Up", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key([mod, "shift"], "Return", lazy.layout.toggle_split(), desc="Toggle between split and unsplit sides of stack"),
    
    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "Escape", lazy.window.kill(), desc="Kill focused window"),
	
    # Session Keybindings
    Key([mod], "End", lazy.spawn(lock), desc="Launch lock screen"),
    Key([mod, "control"], "r", lazy.restart(), desc="Restart Qtile"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    Key([mod], "r", lazy.spawncmd(), desc="Spawn a command using a prompt widget"),
	
    # Application Keybindings
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),
    Key([mod], "space", lazy.spawn(filemanager), desc="Launch filemanager"),
    Key([mod], "b", lazy.spawn(browser), desc="Launch browser"),
    Key([mod, "shift"], "Menu", lazy.spawn(dmenu), desc="Launch dmenu"),
    Key([mod], "Menu", lazy.spawn(menu), desc="Launch rofi menu"),
    
]

########################################################################
###   WORKSPACES
########################################################################

workspaces = [
    {"name": "","key": "1"},
    {
        "name": "",
        "key": "2",
        "matches": [
			Match(wm_class="firefox"),
			Match(wm_class="Thunderbird")
		],
    },
    {
        "name": "",
        "key": "3",
        "matches": [
            Match(wm_class="libreoffice"),
            Match(wm_class="evince"),
        ],
    },
    {
		"name": "", 
		"key": "4", 
		"matches": [
			Match(wm_class="VSCodium")
		],
	},
    {"name": "", "key": "5", "matches": [Match(wm_class="spotify")]},
    {
        "name": "",
        "key": "6",
        "matches": [
            Match(wm_class="zoom"),
            Match(wm_class="vlc"),
            Match(wm_class="mpv"),
            Match(wm_class="VidCutter"),
        ],
    },
    {"name": "", "key": "7", "matches": [Match(wm_class="hexchat")]},
    {
		"name": "", 
		"key": "8", 
		"matches": [
			Match(wm_class="gimp"),
			Match(wm_class="ristretto")
		]
	},
    {"name": "", "key": "9", "matches": [Match(wm_class="transmission")]},
    {
        "name": "",
        "key": "0",
        "matches": [
            Match(wm_class="lxappearance"),
            Match(wm_class="pavucontrol"),
            Match(wm_class="connman-gtk"),
        ],
    },
]

groups = []


for workspace in workspaces:
    matches = workspace["matches"] if "matches" in workspace else None
    groups.append(Group(workspace["name"], matches=matches, layout="bsp"))
    keys.append(
        Key(
            [mod],
            workspace["key"],
            lazy.group[workspace["name"]].toscreen(),
            desc="Focus this desktop",
        )
    )
    keys.append(
        Key(
            [mod, "shift"],
            workspace["key"],
            lazy.window.togroup(workspace["name"]),
            desc="Move focused window to another group",
        )
    )


########################################################################
###   LAYOUT
########################################################################


layout_theme = {
    "border_width": 2,
    "margin": 9,
    "border_focus": "#5AF78E",
    "border_normal": "#CAA9FA",
    "font": "FiraCode Nerd Font",
    "grow_amount": 2,
}

layouts = [
    layout.Bsp(name='[bsp]', **layout_theme),
    layout.Matrix(name='[matrix]', **layout_theme),
    layout.Tile(name='[tile]', **layout_theme),
    layout.Floating(name='[floating]', **layout_theme),
]

########################################################################
###   MOUSE CALLBACK FUNCTIONS
########################################################################

def open_calendario():
	qtile.cmd_spawn(terminal + ' -e gcalcli agenda')
	
def open_volume():
	qtile.cmd_spawn('pavucontrol')

########################################################################
###   WIDGET
########################################################################

widget_defaults = dict(
    font='RobotoMono Nerd Font',
    background="#282A36",
    fontsize=12,
    padding=3,
)

extension_defaults = widget_defaults.copy()

group_box_settings = {
    "borderwidth": 3,
    "active": '#CAA9FA',
    "inactive": 'BFBFBF',
    "disable_drag": True,
    "rounded": False,
    "block_highlight_text_color": '#50FA7B',
    "highlight_method": "line",
    "this_current_screen_border":'#50FA7B',
    "foreground": '#F8F8F2',
    "background": '#282A36',
    "urgent_border": '#FF5555',
}

prompt_settings = {
	"cursor_color": '#CAA9FA',
	"foreground": '#50FA7B',
	"prompt": '  ',
}

current_layout_setting = {
	"foreground": '#8BE9FD',
}

window_name_settings = {
	"foreground": '#50FA7B',
	"max_chars":50,
}

quick_exit_settings = {
	"default_text": '⏻',
	"countdown_format":'{}',
	"fontsize": 16,
	"foreground": '#FF5555', 
}

check_update_settings = {
	"distro": 'Arch',
	"display_format": ' {updates}',
	"colour_have_updates": '#5AF78E',
	"colour_no_updates": '#BFBFBF',
	"no_update_string": 'OK',
	"update_interval":3600,
}

wlan_settings = {
	"interface": 'wlan0',
	"format": '{essid}{pecent:2.0%}',
}

battery_settings = {
	"fontsize": 16,
	"battery":0,
	"charge_char": '',
	"discharge_char": '',
	"empty_char": '',
	"full_char": '',
	"format": '{char} {percent:2.0%}',
	"low_foreground": '#FF5555',
	"low_percentage": 0.15,
	"notify_below": 0.15,
}

screens = [
    Screen(
        top=bar.Bar(
            [
                widget.GroupBox(fontsize=16,**group_box_settings),
                widget.CurrentLayout(**current_layout_setting),
                widget.Prompt(**prompt_settings),
                widget.Sep(linewidth=0,padding=10),
                widget.WindowName(**window_name_settings),
                widget.TextBox(text="",fontsize=16,foreground="#CAA9FA",padding=5),
                widget.Clock(format='%a %I:%M %p',mouse_callbacks={'Button1': open_calendario}),
                widget.Sep(linewidth=0,padding=10),
                widget.TextBox(text="",fontsize=16,foreground="#CAA9FA",padding=5),
                widget.CheckUpdates(**check_update_settings),
                widget.Sep(linewidth=0,padding=10),
                widget.TextBox(text="",fontsize=16,foreground="#CAA9FA",padding=5,mouse_callbacks={'Button3':open_volume}),
                widget.PulseVolume(mouse_callbacks={'Button3': open_volume}),
                widget.Sep(linewidth=0,padding=10),
                widget.TextBox(text="",fontsize=16,foreground="#CAA9FA",padding=5),
                widget.Wlan(**wlan_settings),
                widget.Sep(linewidth=0,padding=10),
                widget.TextBox(text="",fontsize=16,foreground="#CAA9FA",padding=5),
                widget.Battery(**battery_settings),
                widget.Systray(),
                widget.Sep(linewidth=0,padding=10),
                widget.QuickExit(padding=5,**quick_exit_settings),
            ],
            24,
        ),
    ),
]

########################################################################
###   MOUSE
########################################################################

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

########################################################################
###   GENERAL
########################################################################

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None  # WARNING: this is deprecated and will be removed soon
follow_mouse_focus = False
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    *layout.Floating.default_float_rules,
    Match(wm_type="utility"),
	Match(wm_type="notification"),
	Match(wm_type="toolbar"),
	Match(wm_type="splash"),
	Match(wm_type="dialog"),
	Match(wm_class="confirm"),
	Match(wm_class="dialog"),
	Match(wm_class="download"),
	Match(wm_class="error"),
	Match(wm_class="file_progress"),
	Match(wm_class="notification"),
	Match(wm_class="splash"),
	Match(wm_class="toolbar"),
	Match(wm_class="pomotroid"),
	Match(wm_class="cmatrixterm"),
	Match(title="Farge"),
	Match(wm_class="feh"),
	Match(wm_class="galculator"),
	Match(wm_class="blueman-manager"),
    Match(wm_class='confirmreset'),  # gitk
    Match(wm_class='makebranch'),  # gitk
    Match(wm_class='maketag'),  # gitk
    Match(wm_class='ssh-askpass'),  # ssh-askpass
    Match(title='branchdialog'),  # gitk
    Match(title='pinentry'),  # GPG key password entry
])
auto_fullscreen = True
focus_on_window_activation = "smart"

########################################################################
###   SPECIAL FUNCTION
########################################################################

# Startup scripts
@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser("~")
    subprocess.call([home + "/.config/qtile/autostart.sh"])

# Go to group when app opens on matched group
@hook.subscribe.client_new
def modify_window(client):
    # if (client.window.get_wm_transient_for() or client.window.get_wm_type() in floating_types):
    #    client.floating = True

    for group in groups:  # follow on auto-move
        match = next((m for m in group.matches if m.compare(client)), None)
        if match:
            targetgroup = client.qtile.groups_map[
                group.name
            ]  # there can be multiple instances of a group
            targetgroup.cmd_toscreen(toggle=False)
            break

# Floating window management
@hook.subscribe.client_new
def floating_dialogs(window):
    dialog = window.window.get_wm_type() == 'dialog'
    transient = window.window.get_wm_transient_for()
    if dialog or transient:
        window.floating = True

@hook.subscribe.client_new
def idle_dialogues(window):
    if((window.window.get_name() == 'Search Dialog') or
      (window.window.get_name() == 'Module') or
      (window.window.get_name() == 'Goto') or
      (window.window.get_name() == 'IDLE Preferences')):
        window.floating = True

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "qtile"
