"****************************
"*                          *
"*   Vimcrc by Undermod     *
"*                          *
"****************************

" ---------- IMPOSTAZIONI GENERALI ----------

"Scrive le modifiche prima di cambiare buffer
set autowrite

"Disattiviamo i backupfile
set nobackup
set nowritebackup
set noswapfile

"Cronologia dei comandi
set history=50

"Abilitazione modalita' copia-incolla
set paste

"Evidenziare termini trovati con la ricerca
set hlsearch

"Disabilitiamo il wrap delle linee
set nowrap

"Retro compatibilita' disabilitata
set nocompatible

"Mostiramo i numeri di riga
set number

"Mostriamo le coordinate del cursore
set ruler

"Mostra output comandi in barra di stato
set showcmd

"Disabilitiamo il wrap delle parole
set textwidth=0

"Settiamo il numero di undo
set undolevels=50

"Menu per il completamento dei comandi
set wildmenu

"Forza apertura file con permessi di sudo
cmap w!! %!sudo tee > /dev/null %

" ---------- MAPPATURA TASTIERA ----------

"Trascinare riga in alto in basso con CTRL+arrow
nmap <C-Up> ddkP
nmap <C-Down> ddp

"Spostamento tra le varie finestre con ALT+arrow
nmap <silent> <A-Up> :wincmd k<CR>
nmap <silent> <A-Down> :wincmd j<CR>
nmap <silent> <A-Left> :wincmd h<CR>
nmap <silent> <A-Right> :wincmd l<CR>

"Keybinging x NerdTree
map <C-n> :NERDTreeToggle<CR>

" ---------- CARATTERI ----------

set encoding=utf8
set guifont=Droid\ Sans\ Mono\ for\ Powerline\ Plus\ Nerd\ File\ Types\ 11

" ---------- SCHEMI COLORI ----------

"Impostazione per sfondo del teminale scuro
set bg=dark

"Impostazione schema colori
syntax on
set t_Co=256
set termguicolors

" ---------- STATUSBAR ----------

"Impostiamo statusbar sempre visibile
set laststatus=2

"Cambio colore per la modalita' inserimento
if &term=~ "xterm"
	let &t_SI = "\<Esc>]12;cyan\x7"
	let &t_EI = "\<Esc>]12;white\x7"
endif

"Info della Statusbar
set statusline=%F%m%r%h%w\ %y\ [row=%l/%L]\ [col=%02v]\ [%02p%%]\
set statusline+=%#warningmsg#
"set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

" ---------- PROGRAMMAZIONE ----------

"Colorazione del testo in base alla sintassi
"syntax on
syntax enable
let python_highlight_all=1

"Abilitiamo la autoindentazione
set autoindent

"Usare gli spazi invece della tabulazione
set expandtab

"N° di spazi da sostituire in autoindentazione
set shiftwidth=4

"Evidenziamo le parentesi corrispondenti
set showmatch

"Sostituiamo il carattere tab con spazi
set tabstop=4
set softtabstop=4

"Impostiamo il fileformat
set fileformat=unix

" Code Folding
set foldmethod=indent
set foldlevel=99

" Abilitiamo il code folding con lo spazio
nnoremap <space> za

" ---------- CONFIGURAZIONE PLUGIN ----------

" Attivazione Tagbar
nmap <F8> :TagbarToggle<CR>

" Configurazione Tema Airline
"let g:airline_theme='onedark'
let g:airline_theme='dracula'
let g:airline_powerline_fonts=1

" Configurazione Deoplete
let g:deoplete#enable_at_startup = 1
" Chiusura automatica dei suggerimenti
autocmd InsertLeave,CompleteDone * if pumvisible() == 0 | pclose | endif
" Navigazione tra i suggerimenti con TAB
inoremap <expr><tab> pumvisible() ? "\<c-n>" : "\<tab>"

" Configurazione Neomake
let g:neomake_python_enabled_makers = ['pylint']
" Configurazione Syntastic
"let g:syntastic_always_populate_loc_list = 1
"let g:syntastic_auto_loc_list = 1
"let g:syntastic_check_on_open = 1
"let g:syntastic_check_on_wq = 0

" ---------- ATTIVAZIONE PLUGIN -----------

" --- Gestione Plugin ---"
call plug#begin('~/local/share/nvim/plugged')
" --- Utility  --- "
Plug 'scrooloose/nerdtree'
Plug 'majutsushi/tagbar'
Plug 'tmhedberg/SimpylFold'
Plug 'jiangmiao/auto-pairs'
" --- Aspetto ---"
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'ryanoasis/vim-devicons'
Plug 'norcalli/nvim-colorizer.lua'
"Plug 'joshdick/onedark.vim'
Plug 'dracula/vim',{'as':'dracula'}
"--- Git ---"
Plug 'tpope/vim-fugitive'
" --- Sintassi --- "
"Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins'}
"Plug 'zchee/deoplete-jedi'
"Plug 'neomake/neomake'
"Plug 'carlitux/deoplete-ternjs', { 'do': 'npm install -g tern' }
"Plug 'pangloss/vim-javascript'
"Plug 'mattn/emmet-vim'
"Plug 'elzr/vim-json'
"Plug 'vim-syntastic/syntastic'
"Plug 'klen/python-mode'
call plug#end()

" COLORSCHEME ATTIVATION
"colorscheme onedark
colorscheme dracula

" COLORIZER ATTIVATION
lua require'plug-colorizer'
