#!/bin/sh

# E' necessario abilitare pacman ad essere eseguito senza password

icon="/usr/share/icons/Tela/24/actions/dialog-information.svg"
list=$(sudo dnf list --upgrades | wc -l)
update=$(echo $list 1 | awk '{print $1 - $2}')

if [[ $update -eq 0 ]]
then
    notify-send -i $icon 'RILASSATI' 'Non ci sono aggiornamenti'
fi
if [[ $update -ne 0 ]]
then
    sudo dnf upgrade -y
    notify-send -i $icon 'REPO' 'Aggiornamenti effettuati'
fi
polybar-msg action "#updates.hook.0"
