#!/bin/sh

# E' necessario abilitare pacman ad essere eseguito senza password

repo=$(pacman -Qu | wc -l)
aur=$(paru -Qua | wc -l)

echo $repo 

if [[ $repo -ne 0 ]] || [[ $aur -ne 0 ]]
then
    notify-send -i "/usr/share/icons/Tela/24/actions/dialog-information.svg" 'Check Repository' 'Ci sono aggiornamenti da effettuare'
fi
if [[ $repo -eq 0 ]]
then
	if [[ $aur -eq 0 ]]
	then
		echo -e "%{F#8be9fd} 0 %{F#00ff84} 0%{F-}"
	else
		echo -e "%{F#8be9fd} 0 %{F#ff0099} $aur%{F-}"
	fi
fi 
if [[ $repo -ne 0 ]]
then
	if [[ $aur -eq 0 ]]
	then
		echo -e "%{F#ff0099} $repo %{F#00ff84} 0%{F-}"
	else
		echo -e "%{F#ff0099} $repo  $aur%{F-}"
	fi
fi
