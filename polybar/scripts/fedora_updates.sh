#!/bin/sh

# E' necessario abilitare pacman ad essere eseguito senza password

list=$(sudo dnf list --upgrades | wc -l)
update=$(echo $list 1 | awk '{print $1 - $2}')

if [[ $update -ne 0 ]]
then
    echo -e "%{F#ff0099}  $update %{F-}"
    notify-send -i "/usr/share/icons/Tela/scalable/apps/system-restart.svg" 'Aggiornamento Pacchetti' 'Ci sono aggiornamenti da effettuare'
else
	echo -e "%{F#8be9fd} %{F-}"
fi