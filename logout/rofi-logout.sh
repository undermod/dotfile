#!/bin/bash

domanda=$(echo "  BLOCCA |  LOGOUT |  RIAVVIA |  SPEGNI" | rofi -sep "|"\
    -dmenu -p "Cosa vuoi fare? " -width 25 -hide-scrollbar \
    -line-padding 2 -lines 2 -padding 200 -color-enabled \
    -columns 2 -opacity 95 -fullscreen false)

case $domanda in
    *BLOCCA)
        betterlockscreen -l dim        
        ;;
    *LOGOUT)
        bsqc quit
        ;;
    *RIAVVIA)
        systemctl reboot
        ;;
    *SPEGNI)
        systemctl poweroff
        ;;
    *)
        exit 0  # do nothing on wrong response
        ;;
esac